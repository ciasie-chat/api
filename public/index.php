<?php
require '../vendor/autoload.php';
require '../config.dist.php';

define('ROOTPATH', dirname(__FILE__) . "/../");

$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection(['driver' => 'mysql',
    'host' => DB_HOST,
    'database' => DB_BASE,
    'username' => DB_USER,
    'password' => DB_PASS,
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_general_ci',
    'prefix' => '']);
$db->setAsGlobal();
$db->bootEloquent();


$c = new \Slim\Container([
    'settings' => [
        'displayErrorDetails' => DEV_MODE
    ]
]);

$app = new \Slim\App($c);

$app->get('/ping[/]', '\app\controllers\GeneralController:ping');
$app->get('/user[/]', '\app\controllers\UserController:all')->add(\app\middlewares\AuthRequiredMiddleware::class);
$app->post('/user[/]', '\app\controllers\UserController:insert')->add(\app\middlewares\UserMiddleware::class);
$app->get('/user/login[/]', '\app\controllers\UserController:login');
$app->get('/user/checkToken[/]', '\app\controllers\UserController:checkToken');

$app->post('/message[/]', '\app\controllers\MessageController:insert')->add(\app\middlewares\AuthRequiredMiddleware::class)->add(\app\middlewares\MessageMiddleware::class);
$app->get('/message[/]', '\app\controllers\MessageController:all')->add(\app\middlewares\AuthRequiredMiddleware::class);

$c['notAllowedHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withStatus(405)
            ->withHeader('Content-Type', 'application/json')
            ->write(\core\Utils::jsonError("Method Not Allowed"));
    };
};

$c['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withStatus(404)
            ->withHeader('Content-Type', 'application/json')
            ->write(\core\Utils::jsonError("Not Found"));
    };
};

$app->run();