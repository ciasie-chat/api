var session = null;
var socket = null;
var users = [];

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

const axiosChat = axios.create({
    baseURL: api_base_url,
    timeout: 1000
});

$('#login button').click(() => {
    axiosChat.get('/user/login', {
        auth: {
            username: $('#login #inputUsername').val(),
            password: $('#login #inputPassword').val()
        }
    }).then((res) => {
        session = res.data.data;
        $('#login').hide();
        $('#register').hide();
        startChat();
    }).catch((err) => {
        if (err.response.status == 401) {
            showNotice('Le nom d\'utilisateur ou le mot de passe est incorrect', 'danger', 3000);
        } else {
            showNotice('Une erreur est survenue', 'danger');
        }
    })
});

$('#register button').click(() => {

    if ($('#register #inputPassword').val() == $('#register #inputPassword2').val()) {
        axiosChat.post('/user', {
            username: $('#register #inputUsername').val(),
            password: $('#register #inputPassword').val()
        }).then((res) => {
            showNotice('Inscription réussie, vous pouvez désormais vous connecter !', 'success', 3000);
            $('#register').hide();
        }).catch((err) => {
            if (err.response.status == 400) {
                showNotice('Votre mot de passe doit contenir au minimum 4 caractères', 'danger', 3000);
            } else if (err.response.status == 409) {
                showNotice('Ce nom d\'utilisateur existe déjà', 'danger', 3000);
            }
        })
    } else {
        showNotice('Les mots de passe ne correspondent pas', 'danger', 3000);
    }
});

let startChat = () => {
    axiosChat.get('/message', {
        headers: {Authorization: `Bearer ${session.token}`}
    }).then((res) => {
        res.data.data.forEach((e) => {
            createMessage(e);
        })
    });

    axiosChat.get('/user?online', {
        headers: {Authorization: `Bearer ${session.token}`}
    }).then((res) => {
        console.log(res);
        res.data.data.forEach((e) => {
            updateUsersList('connection', e.username);
        })
    });

    try {
        socket = new WebSocket(websocket + "?session_token=" + session.token);
    } catch (exception) {
        console.error(exception);
    }

    socket.onerror = function (error) {
        console.error(error);
        $('#critical').show();
    };

    socket.onopen = function (event) {
        console.log('Connexion au chat établie');
        $('#chat').show();

        this.onclose = function (event) {
            console.log('Connexion au chat terminée');
            $('#chat').hide();
            showNotice('La connexion au chat est terminée', 'info');
        };

        this.onmessage = function (event) {
            let res = JSON.parse(event.data);
            console.log(res);
            if (res && res.status == 'success') {
                if (res.type == "new_message") {
                    createMessage(res.data);
                } else if (res.type == "connection" || res.type == "disconnection") {
                    updateUsersList(res.type, res.data.username);
                    createEvent(res.type, res.data);
                }
            }
        };

        let sendMessage = () => {
            this.send(JSON.stringify({
                message: $('#chat #inputMessage').val()
            }));
            $('#chat #inputMessage').val("");
        };

        $('#chat #inputMessage').keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                event.preventDefault();
                sendMessage();
            }
            event.stopPropagation();
        });

        $('#chat button').click(() => {
            sendMessage();
        });
    };
};

let updateUsersList = (type, username) => {
    if (users.includes(username)) {
        if (type == "disconnection") {
            users.remove(username);
        }
    } else {
        users.push(username)
    }
    $('#users').html(users.join(', '));
};

let createMessage = (message) => {
    let newMessageDiv = $('.message-template').clone().removeClass('message-template');
    $(newMessageDiv).html($(newMessageDiv).html().replace("username", message.user.username));
    $(newMessageDiv).html($(newMessageDiv).html().replace("message", message.content));
    $(newMessageDiv).html($(newMessageDiv).html().replace("time", message.created_at));
    if (message.user.id == session.user_id) {
        $(newMessageDiv).addClass('message-self')
    } else {
        $(newMessageDiv).addClass('message-remote')
    }
    $(newMessageDiv).show().appendTo("#messages");
    $("#messages").animate({scrollTop: 20000000}, "slow");
};

let createEvent = (type, data) => {
    let event_message = "";
    if (type == "connection") {
        event_message = data.username + " a rejoint la conversation";
    } else if (type == "disconnection") {
        event_message = data.username + " a quitté la conversation";
    }

    let newEventDiv = $('.event-template').clone().removeClass('event-template');
    $(newEventDiv).html($(newEventDiv).html().replace("event_message", event_message));
    $(newEventDiv).show().appendTo("#messages");
    $("#messages").animate({scrollTop: 20000000}, "slow");
};

let showNotice = (message, type = 'info', time = null) => {
    $('#notice').html(message);
    $('#notice').attr('class', 'alert alert-' + type);
    $('#notice').show();
    if (time != null) {
        setTimeout(() => {
            $('#notice').hide();
        }, time);
    }
};