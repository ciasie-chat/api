<?php

namespace core;

class Utils
{
    public static function jsonOk($data = [], $message = null)
    {
        $res = ["status" => "success"];

        if ($data != []) {
            $res["data"] = $data;
        }
        if ($message != null) {
            $res["message"] = $message;
        }
        return json_encode($res);
    }

    public static function jsonError($message)
    {
        return json_encode(['status' => 'error', 'message' => $message]);
    }

    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    public static function secure($data)
    {
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $data[$k] = self::secure($data[$k]);
            }
            return $data;
        } elseif (is_object($data)) {
            foreach ($data as $k => $v) {
                $data->$k = self::secure($data->$k);
            }
            $classname = get_class($data);
            $ref = new ReflectionClass($classname);
            $props = $ref->getProperties(ReflectionProperty::IS_PRIVATE | ReflectionProperty::IS_PROTECTED);
            foreach ($props as $prop) {
                $getter = 'get' . self::fromSnakeCaseToCamelCase($prop->getName());
                $setter = 'set' . self::fromSnakeCaseToCamelCase($prop->getName());
                if (method_exists($data, $getter) && method_exists($data, $setter)) {
                    $data->$setter(self::secure($data->$getter()));
                }
            }
            return $data;
        } elseif (!is_string($data)) {
            return $data;
        } else {
            $data = htmlentities($data);
            return $data;
        }
    }

    public static function getBearerToken($req){
        if(isset($req->getHeaders()["HTTP_AUTHORIZATION"])){
            return str_replace("Bearer ", "", $req->getHeaders()["HTTP_AUTHORIZATION"]);
        }
    }
}