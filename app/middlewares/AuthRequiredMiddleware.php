<?php
namespace app\middlewares;

use app\models\Session;
use core\Utils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Psr7\Response;

class AuthRequiredMiddleware
{
    public function __invoke(Request $req, ResponseInterface  $resp,  $next)
    {
        if(isset($req->getHeaders()["HTTP_AUTHORIZATION"])){
            $token = str_replace("Bearer ", "", $req->getHeaders()["HTTP_AUTHORIZATION"]);
            $session = Session::where("token", "=", $token)->get()->first();
            if($session){
                return $next($req, $resp);
            }else{
                $resp_body = Utils::jsonError("Session expired");
            }
        }else{
            $resp_body = Utils::jsonError("Missing headers: Authorization");
        }

        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp = $resp->withStatus(401);
        $resp->getBody()->write($resp_body);
        return $resp;
    }
}
