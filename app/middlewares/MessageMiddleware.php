<?php
namespace app\middlewares;

use core\Utils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Psr7\Response;

class MessageMiddleware
{
    public function __invoke(Request $req, ResponseInterface  $resp,  $next)
    {
        $body = $req->getParsedBody();
        if($body) {

            if (isset($body["content"]) && $body["content"] != "") {
                //Middleware passed
            } else {
                $error = "Missing parameter: content";
            }

        }else{
            $error = "JSON invalid";
        }

        if(isset($error)){
            $resp = $resp->withHeader('Content-Type', 'application/json');
            $resp = $resp->withStatus(400);
            $resp->getBody()->write(Utils::jsonError($error));
            return $resp;
        }else{
            return $next($req, $resp);
        }
    }
}
