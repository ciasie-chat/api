<?php
namespace app\middlewares;

use core\Utils;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Psr7\Response;

class UserMiddleware
{
    public function __invoke(Request $req, ResponseInterface  $resp,  $next)
    {
        $body = $req->getParsedBody();
        if($body){
            if(isset($body["username"]) && isset($body["password"])){
                if(strlen($body["username"]) > 2 && strlen($body["username"]) <= 30){
                    if(strlen($body["password"]) > 4){
                        //Middleware passed
                    }else{
                        $error = "Your password must be longer than 4 characters";
                    }
                }else{
                    $error = "Your username must be between 3 and 30 characters";
                }
            }else{
                $error = "You must enter username, password";
            }
        }else{
            $error = "Invalid JSON";
        }

        if(isset($error)){
            $resp = $resp->withHeader('Content-Type', 'application/json');
            $resp = $resp->withStatus(400);
            $resp->getBody()->write(Utils::jsonError($error));
            return $resp;
        }else{
            return $next($req, $resp);
        }
    }
}
