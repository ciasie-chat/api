<?php
namespace app\controllers;

use app\models\Message;
use app\models\Session;
use core\Utils;

class MessageController{
    public function insert($req, $resp){
        $body = $req->getParsedBody();
        $session = Session::where("token", "=", Utils::getBearerToken($req))->get()->first();

        if(trim($body["content"]) != ""){
            $message = new Message();
            $message->content = Utils::secure($body["content"]);
            $message->user_id = $session->user_id;
            $message->created_at = date('Y-m-d H:i:s');

            $message->save();
        }

        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp = $resp->withStatus(201);
        $resp->getBody()->write(Utils::jsonOk($message));

        return $resp;
    }

    public function all($req, $resp){
        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp = $resp->withStatus(200);
        $messages = array_reverse(Message::latest()->take(5)->get()->toArray());
        $resp->getBody()->write(Utils::jsonOk($messages));

        return $resp;
    }
}