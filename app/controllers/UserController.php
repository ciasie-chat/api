<?php
namespace app\controllers;

use app\models\Session;
use app\models\User;
use core\Utils;
use Ramsey\Uuid\Uuid;

class UserController{
    public function insert($req, $resp){
        $body = $req->getParsedBody();

        $username = Utils::secure($body["username"]);
        if(!User::where('username', '=', $username)->exists()){
            $user = new User();
            $user->username = $username;
            $user->password = password_hash($body["password"], PASSWORD_DEFAULT);

            $user->save();

            $resp = $resp->withStatus(201);
            $resp_body = Utils::jsonOk($user);
        }else{
            $resp = $resp->withStatus(409);
            $resp_body = Utils::jsonError("A user already exists");
        }

        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp->getBody()->write($resp_body);
        return $resp;
    }

    public function login($req, $resp){
        if(isset($req->getHeaders()["HTTP_AUTHORIZATION"])){
            $base64 = str_replace("Basic ","",$req->getHeaders()["HTTP_AUTHORIZATION"])[0];
            $decoded_auth = base64_decode($base64);
            $auth = explode(':', $decoded_auth);

            $user = User::where("username", "=" , $auth[0])->get()->first();
            if($user){
                if(password_verify($auth[1], $user->password)){

                    $session = $user->createSession();

                    $resp = $resp->withStatus(200);
                    $resp_body = Utils::jsonOk($session);
                }else{
                    $resp = $resp->withStatus(401);
                    $resp_body = Utils::jsonError("Incorrect password");
                }
            }else{
                $resp = $resp->withStatus(401);
                $resp_body = Utils::jsonError("The user does not exist");
            }
        }else{
            $resp = $resp->withStatus(400);
            $resp_body = Utils::jsonError("Missing authorization");
        }

        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp->getBody()->write($resp_body);
        return $resp;
    }

    public function checkToken($req, $resp){
        if(isset($_GET["token"])){
            $session = Session::where("token", "=", $_GET["token"])->get()->first();
            if($session){
                $resp = $resp->withStatus(200);
                $resp_body = Utils::jsonOk($session);
            }else{
                $resp = $resp->withStatus(401);
                $resp_body = Utils::jsonError("Session expired");
            }
        }else{
            $resp = $resp->withStatus(400);
            $resp_body = Utils::jsonError("Missing parameters: token");
        }

        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp->getBody()->write($resp_body);
        return $resp;
    }

    public function all($req, $resp){
        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp = $resp->withStatus(200);
        if(isset($_GET['online'])){
            $resp->getBody()->write(Utils::jsonOk(User::where('online', true)->get()));
        }else{
            $resp->getBody()->write(Utils::jsonOk(User::all()));
        }

        return $resp;
    }
}