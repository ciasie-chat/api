<?php
namespace app\controllers;

use core\Utils;

class GeneralController{
    public function ping($req, $resp){
        $resp = $resp->withHeader('Content-Type', 'application/json');
        $resp->getBody()->write(Utils::jsonOk([], "Pong !"));
        return $resp;
    }
}