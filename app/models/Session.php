<?php

namespace app\models;

use core\Utils;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = "session";
    public $timestamps = false;
    protected $hidden = [
        "password"
    ];

    public function createMessage($content){
        $message = new Message();
        $message->content = htmlentities($content);
        $message->user_id = $this->user_id;
        $message->created_at = date('Y-m-d H:i:s');
        $message->save();
        return $message;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

}