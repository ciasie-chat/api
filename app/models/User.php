<?php
namespace app\models;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class User extends Model {
    protected $table = "user";
    public $timestamps = false;
    protected $hidden = [
        "password"
    ];

    public function createSession(){
        $session = new Session();
        $session->user_id = $this->id;
        $session->token = Uuid::uuid1()->toString();
        $session->created_at = date('Y-m-d H:i:s');
        $session->save();

        return $session;
    }

    public function setOnline($online){
        $this->online = $online;
        $this->save();
    }
}