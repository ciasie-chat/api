<?php
namespace app\models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {
    protected $with = ['user'];
    protected $hidden = ['user_id'];
    protected $table = "message";
    public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public static function createWithSession($session, $message){

        return $message;
    }
}