<?php
require 'vendor/autoload.php';
require 'config.dist.php';

use app\models\Message;
use core\Utils;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

define('APP_PORT', 8080);

$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection(['driver' => 'mysql',
    'host' => DB_HOST,
    'database' => DB_BASE,
    'username' => DB_USER,
    'password' => DB_PASS,
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_general_ci',
    'prefix' => '']);
$db->setAsGlobal();
$db->bootEloquent();


class ServerImpl implements MessageComponentInterface
{
    protected $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->clients_sessions = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        echo "New connection! ({$conn->resourceId}).\n";

        $querystring = $conn->httpRequest->getUri()->getQuery();
        parse_str($querystring, $queryarray);
        if(isset($queryarray['session_token'])){
            $session = \app\models\Session::where('token', $queryarray['session_token'])->get()->first();
            if ($session) {
                $this->clients->attach($conn);
                $this->clients_sessions[$conn->resourceId] = $session->token;
                $session->user->setOnline(true);

                foreach ($this->clients as $client) {
                    $client->send(json_encode(['type' => 'connection', 'status' => 'success', 'data' => \app\models\Session::where('token', $this->clients_sessions[$conn->resourceId])->get()->first()->user]));
                }
            }
        }
    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {
        echo sprintf("New message from '%s': %s\n\n\n", $conn->resourceId, $msg);

        if (isset($this->clients_sessions[$conn->resourceId])) {
            $session = \app\models\Session::where('token', $this->clients_sessions[$conn->resourceId])->get()->first();
            if ($session && \core\Utils::isJson($msg)) {
                $data = json_decode($msg, true);
                if (isset($data['message']) && trim($data["message"]) != "") {
                    $message = $session->createMessage($data["message"]);
                    foreach ($this->clients as $client) {
                        $client->send(json_encode(['type' => 'new_message', 'status' => 'success', 'data' => array_merge($message->toArray(), ['user' => $message->user()->get()->toArray()[0]])]));
                    }
                }
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        echo "Connection {$conn->resourceId} is gone.\n";
        if(isset($this->clients_sessions[$conn->resourceId])){
            $session = \app\models\Session::where('token', $this->clients_sessions[$conn->resourceId])->get()->first();
            foreach ($this->clients as $client) {
                $client->send(json_encode(['type' => 'disconnection', 'status' => 'success', 'data' => $session->user]));
            }
            $session->user->setOnline(false);
        }
        $this->clients->detach($conn);
        
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error occured on connection {$conn->resourceId}: {$e->getMessage()}\n\n\n";
        $conn->close();
    }
}

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new ServerImpl()
        )
    ),
    APP_PORT
);
echo "Server created on port " . APP_PORT . "\n\n";
$server->run();